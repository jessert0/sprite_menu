#!/usr/bin/env python

import pygame
from ui import UI
from player import Player


class Status(pygame.sprite.Sprite):
    active = False
    on = 0

    def __init__(self, x_pos, y_pos):
        super().__init__()
        self.image = pygame.Surface((380, 155), pygame.SRCALPHA)
        self.image.fill((0, 0, 0, 125))
        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos
        self.textsurface, self.textrect, self.font_size = UI.text_rend("Player position", 20, 10, 10)

    def update(self):
        self.image.fill((0, 0, 0, 125))
        self.textsurface, self.textrect = UI.text_rend("Player position:", 20, 168, 10)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))

        self.textsurface, self.textrect = UI.text_rend("center:", 20, 180, 85)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))
        self.textsurface, self.textrect = UI.text_rend(Player.p_pos, 20, 230, 85)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))

        self.textsurface, self.textrect = UI.text_rend("left:", 20, 100, 85)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))
        self.textsurface, self.textrect = UI.text_rend(Player.h_pos_left, 20, 130, 85)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))

        self.textsurface, self.textrect = UI.text_rend("right:", 20, 290, 85)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))
        self.textsurface, self.textrect = UI.text_rend(Player.h_pos_right, 20, 330, 85)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))

        self.textsurface, self.textrect = UI.text_rend("up:", 20, 180, 40)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))
        self.textsurface, self.textrect = UI.text_rend(Player.v_pos_up, 20, 230, 40)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))

        self.textsurface, self.textrect = UI.text_rend("down:", 20, 180, 130)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))
        self.textsurface, self.textrect = UI.text_rend(Player.v_pos_down, 20, 230, 130)
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))

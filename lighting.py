#!/usr/bin/env python
import pygame

class Light(pygame.sprite.Sprite):

    def __init__(self, x_pos, y_pos):
        super().__init__()
        self.image = pygame.Surface((800, 600), pygame.SRCALPHA)
        self.image.fill((0, 0, 0, 150))
        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos
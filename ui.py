#!/usr/bin/python3.6

import pygame
from tiles import Tile

pygame.init()
pygame.font.init()


class UI(pygame.sprite.Sprite):
    block_type = Tile.grass

    def __init__(self, x_pos, y_pos, message, block_type):
        super().__init__()
        self.image = pygame.Surface((200, 300), pygame.SRCALPHA)   # set the surface to per pixel alpha
        self.image.fill((0, 0, 0, 125))     # alpha is the forth integer for per pixel alpha
        # self.image.set_alpha(125)
        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos
        self.btn = pygame.Surface((105, 40))  # make a surface with normal alpha
        self.btn.fill((77, 166, 255))  # Cornflower Blue
        self.btn.set_alpha(250)
        self.btn_rect = self.btn.get_rect()
        self.btn_rect.x = self.rect.x + 50
        self.btn_rect.y = 400
        self.block_type = block_type
        self.message = message
        self.y = y_pos
        self.image.blit(self.btn, (50, 100))
        self.textsurface, self.textrect, self.font_size = UI.text_rend('Press return', 25, 52, 110)

    def update(self, arg1, arg2):
        self.image.blit(self.textsurface, (self.textrect.x, self.textrect.y))      # Blit 'Press return' onto the button surface
        if self.rect.collidepoint(arg1, arg2):
            if self.rect.y > 300:
                self.rect.y -= 10

        if not self.rect.collidepoint(arg1, arg2):
            if self.rect.y < self.y:
                self.rect.y += 10

    @staticmethod
    def text_rend(mess, size, x, y):
        message = (str(mess))
        myfont = pygame.font.SysFont('Hack', size)

        font_size = myfont.size(mess)   # getting the font size (width and height)
        # print(font_size[0])
        textsurface = myfont.render(message, False, (204, 230, 255))  # Lavender Blue
        textrect = textsurface.get_rect()
        textrect.x = x
        textrect.y = y
        return textsurface, textrect, font_size

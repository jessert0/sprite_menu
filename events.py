#!/usr/bin/python3.6

import pygame
import sys
from player import Player
from tiles import Tile
from status import Status
from screen_text import Screen


def event_list(player, menu_group, mx, my):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                Player.right = True
                if Tile.List[Player.h_pos_right].walkable:
                    player.rect.x += 40
            if event.key == pygame.K_LEFT:
                Player.left = True
                if Tile.List[Player.h_pos_left].walkable:
                    player.rect.x -= 40
            if event.key == pygame.K_UP:
                Player.up = True
                if Tile.List[Player.v_pos_up].walkable:
                    player.rect.y -= 40
            if event.key == pygame.K_DOWN:
                Player.down = True
                if Tile.List[Player.v_pos_down].walkable:
                    player.rect.y += 40
            if event.key == pygame.K_RETURN:
                Screen.enter = True
            if event.key == pygame.K_DELETE:
                if len(Screen.text) > 0:
                    Screen.text = Screen.text[:-1]
                    Screen.txt_hist.pop(-1)     # if a character is deleted remove the last appended entry to txt_hist
            else:
                Screen.text += event.unicode    # storing all key strokes in a variable
                Screen.text1 = event.unicode    # storing a single key stroke in a variable
                Screen.txt_hist.append(Screen.text1)  # temporary storage of 'single keys' into a list

                print(Screen.text)
                print("txt_hist: " + str(Screen.txt_hist))
        if event.type == pygame.MOUSEBUTTONDOWN:
            for block in range(300):
                if Tile.List[block].rect.collidepoint(mx, my):
                    if Tile.List[block].walkable:
                        Tile.List[block].tile_type = Tile.block_type

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                Player.right = False
                player.collision = False
            if event.key == pygame.K_LEFT:
                Player.left = False
                player.collision = False
            if event.key == pygame.K_UP:
                Player.up = False
            if event.key == pygame.K_DOWN:
                Player.down = False
            if event.key == pygame.K_F1:
                Status.active = True
                Status.on += 1
                if Status.on == 2:
                    Status.active = False
                    Status.on = 0

        for menus in menu_group:
            if menus.rect.collidepoint(mx, my):
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        Tile.block_type = menus.block_type
                        menus.image.blit(menus.btn, (50, 100))
'''
        for menus in menu_group:
            if menus.rect.collidepoint(mx, my):
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if menus.btn_rect.collidepoint(mx, my):
                        print(menus.message)
                        Tile.block_type = menus.block_type
                        ## Tile.List[Player.p_pos].tile_type = menus.block_type
                        menus.image.blit(menus.btn, (50, 100))
                        menus.font_parameters = Menu.text_rend('Click', 27)
                if event.type == pygame.MOUSEBUTTONUP:
                    if menus.btn_rect.collidepoint(mx, my):
                        menus.image.blit(menus.btn, (50, 100))
                        menus.font_parameters = Menu.text_rend('Click', 30)
'''

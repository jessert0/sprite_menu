#!/usr/bin/python3.6


from ui import *
import events
from tiles import Tile
from player import Player
from status import Status
from screen_animation import ScreenAnimation
from circle import Circle
from screen_text import Screen

pygame.init()
clock = pygame.time.Clock()

screen = pygame.display.set_mode((800, 600))

menu1 = UI(75, 525, "hello", Tile.pipe)
menu2 = UI(300, 525, "Yay!", Tile.dirt)
menu3 = UI(525, 525, "goodbye", Tile.grass)
player = Player(120, 120)
status = Status(430, 0)
home = ScreenAnimation(0, 0)
block = Circle(410, 170, Tile.dirt, screen)
screen1 = Screen()

solid_group = pygame.sprite.Group()
menu_group = pygame.sprite.Group()
status_group = pygame.sprite.Group()
player_group = pygame.sprite.Group()
home_group = pygame.sprite.Group()
block_group = pygame.sprite.Group()
screen_group = pygame.sprite.Group()

menu_group.add(menu1)
menu_group.add(menu2)
menu_group.add(menu3)
status_group.add(status)
player_group.add(player)
home_group.add(home)
block_group.add(block)
screen_group.add(screen1)

solid_list = Tile.grid_setup(solid_group)

while True:

    home_group.draw(screen)
    home_group.update(screen)
    # block_group.draw(screen)
    block_group.update(screen)
    screen_group.update(screen)
    movement = [0, 0]
    # print("h_no_left  p_no  h_no_right  " + str(Player.h_pos_left) + "  " + str(Player.p_pos) + "  " + str(Player.h_pos_right))
    # print("v_no_up  p_no  v_no_down     " + str(Player.v_pos_up) + "  " + str(Player.p_pos) + "  " + str(Player.v_pos_down))
    mx, my = pygame.mouse.get_pos()
    # Tile.grid_display()
    events.event_list(player, menu_group, mx, my)
    # player_group.draw(screen)
    # player_group.update(solid_group)
    if Status.active:
        status_group.draw(screen)
        status_group.update()
    # menu_group.draw(screen)
    # menu_group.update(mx, my)
    pygame.display.flip()
    clock.tick(60)

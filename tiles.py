#!/usr/bin/python3.6

import pygame

screen = pygame.display.set_mode((800, 600))


class Tile(pygame.sprite.Sprite):
    cnt = 0
    List = []
    tile_id = 0
    width, height = (40, 40)
    block_type = pygame.image.load('imgs/grass40x40.png')

    solids = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 39,
              40, 59, 60, 79, 80, 99, 100, 108, 119, 120, 129, 139, 140, 159, 160,
              179, 180, 199, 200, 219, 220, 239, 240, 259, 260, 279, 280, 281, 282, 283, 284, 285, 286,
              287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299]

    grass = pygame.image.load('imgs/grass40x40.png').convert()
    dirt = pygame.image.load('imgs/dirt40x40.png').convert()
    water = pygame.image.load('imgs/water40x40.png').convert()
    pipe = pygame.image.load('imgs/pipe40x40.png').convert()
    pipe.set_colorkey((0, 0, 0))

    def __init__(self, xpos, ypos, tile_type, walkable):
        super().__init__()
        self.img = pygame.Surface((Tile.width, Tile.height))
        self.rect = self.img.get_rect()
        self.rect.x = xpos
        self.rect.y = ypos
        self.tile_type = tile_type
        self.walkable = walkable
        self.number = Tile.tile_id
        Tile.tile_id += 1

    @staticmethod
    def grid_setup(solid_group):
        for row in range(15):
            for col in range(20):
                sett = 0
                for solid in Tile.solids:
                    if Tile.cnt == solid:
                        walkable = False
                        tile_type = Tile.dirt
                        Tile.List.append(Tile(col * 40, row * 40, tile_type, walkable))
                        tile_solid = Tile(col * 40, row * 40, tile_type, walkable)
                        solid_group.add(tile_solid)
                        sett = 1
                if sett != 1:
                    walkable = True
                    tile_type = Tile.grass
                    Tile.List.append(Tile(col * 40, row * 40, tile_type, walkable))
                Tile.cnt += 1
        return solid_group

    @staticmethod
    def grid_display():
        cnt = 0
        for row in range(15):
            for col in range(20):
                screen.blit(Tile.List[cnt].tile_type, (col * 40, row * 40))
                cnt += 1


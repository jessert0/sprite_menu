#!/usr/bin/env python

import pygame
from ui import UI


class Screen(pygame.sprite.Sprite):
    cnt = 0
    alpha_no_1 = 250
    bottom = 0
    enter = False
    toggle = False
    y = 0
    x = 0
    xy = [0, 0]
    text = ""
    text1 = ""
    txt_hist = []
    txt = ""
    hist = ""
    hist_list = []
    hist_list_loc = []

    def __init__(self):
        super().__init__()
        self.screenprompt = pygame.image.load('imgs/game_bg_screenprompt.png').convert_alpha()
        self.screenprompt_rect = self.screenprompt.get_rect()
        self.screenprompt_rect.x = 0
        self.screenprompt_rect.y = 0

    def update(self, screen):

        # displaying key strokes
        ts, tr, fs = UI.text_rend(Screen.text, 12, 0, 0)
        screen.blit(ts, (34, Screen.y + 314))
        Screen.x = fs[0]  # making the cursor position equal to the width of the font

        # re-blitting key strokes after hitting enter
        if Screen.toggle:
            for i in range(len(Screen.hist_list_loc)):
                ts, tr, fs = UI.text_rend(Screen.hist_list[i], 12, 0, 0)
                screen.blit(ts, (Screen.hist_list_loc[i][0], Screen.hist_list_loc[i][1]))
                # Screen.hist_list_loc[i][1] = Screen.hist_list[i][1] - Screen.bottom
                # Screen.bottom = 0


        # returning the cursor to home position to wrap the line
        if Screen.x > 335:
            Screen.enter = True

        # Return cursor to the left and down to the next line
        if Screen.enter:
            Screen.x = 34  # home position of cursor (x position)
            Screen.xy = [Screen.x, Screen.y + 314]
            Screen.hist = Screen.hist.join(Screen.txt_hist)  # joining the txt_hist list into a string (Screen.hist)
            s2 = slice(-1)  # chop off the return character
            Screen.hist_list.append(Screen.hist[s2])  # appending 'Screen.hist' string into a list
            Screen.hist_list_loc.append(Screen.xy)
            Screen.y += 20  # increment y position - placing cursor on next line
            if Screen.bottom > 260:        # test
                Screen.bottom = 20
            print(Screen.y)

            if Screen.hist[s2] == "clear":
                Screen.x = 0
                Screen.y = 0
                Screen.xy = [0, 0]
                Screen.xy_list = []
                Screen.hist_list = []
                Screen.hist_list_loc = []
            # if Screen.text is not "clear":
            # Screen.text = "command not found"
            # Screen.x = 0
            # Screen.y += 10
            print(Screen.hist_list)
            print(Screen.hist_list_loc)
            # print(Screen.hist_list_loc[0][1])
            # clear variables and lists
            Screen.text = ""
            Screen.hist = ""
            Screen.txt_hist = []
            Screen.enter = False
            Screen.toggle = True

        screen.blit(self.screenprompt, (Screen.x, Screen.y))

        self.screenprompt.set_alpha(Screen.alpha_no_1)  # turning screen prompt on and off

        # Frequency of the screen prompt blink
        if Screen.cnt == 70:
            Screen.alpha_no_1 = 0  # set alpha to 100% clear
        if Screen.cnt == 140:
            Screen.alpha_no_1 = 250  # set alpha to 100% opaque
            Screen.cnt = 0
        Screen.cnt += 2
        Screen.enter = False

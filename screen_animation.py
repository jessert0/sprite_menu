#!/usr/bin/python3

import pygame
import load_assets as ld   # test

images10 = ld.LoadImages()    # pre-load all images test


class ScreenAnimation(pygame.sprite.Sprite):
    angle = 0
    angle1 = 0
    alpha_no = 0
    # alpha_no_1 = 250
    toggle = 0

    def __init__(self, x_pos, y_pos):

        font = pygame.font.Font(None, 50)

        super().__init__()
        self.image = pygame.image.load('imgs/game_bg_decoratorless5.png').convert()
        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos
        self.bg_ticks = pygame.image.load('imgs/game_bg_ticks.png').convert()
        self.bg_ticks.set_colorkey((0, 0, 0))
        self.bg_ticks_rect = self.bg_ticks.get_rect()
        self.bg_ticks_rect.x = x_pos
        self.bg_ticks_rect.y = y_pos
        self.bg_blocks = pygame.image.load('imgs/game_bg_blocks_120x120.png').convert_alpha()
        self.bg_blocks_rect = self.bg_blocks.get_rect()
        self.bg_blocks_rect.x = x_pos
        self.bg_blocks_rect.y = y_pos
        self.bg_circle_left = pygame.image.load('imgs/game_bg_half_circle_left_151x151a.png').convert_alpha()
        self.bg_circle_left_rect = self.bg_circle_left.get_rect()
        self.bg_circle_left_rect.x = x_pos
        self.bg_circle_left_rect.y = y_pos
        self.bg_circle_right = pygame.image.load('imgs/game_bg_half_circle_right_119x119b.png').convert_alpha()
        self.bg_circle_right_rect = self.bg_circle_right.get_rect()
        self.bg_circle_right_rect.x = x_pos
        self.bg_circle_right_rect.y = y_pos
        self.bg_reflection = pygame.image.load('imgs/game_bg_reflection_75x75.png').convert_alpha()
        self.bg_reflection_rect = self.bg_reflection.get_rect()
        self.bg_reflection_rect.x = x_pos
        self.bg_reflection_rect.y = y_pos
        self.bg_reflection_outer = pygame.image.load('imgs/game_bg_reflection_outer_201x201.png').convert_alpha()
        self.bg_reflection_outer_rect = self.bg_reflection.get_rect()
        self.bg_reflection_outer_rect.x = x_pos
        self.bg_reflection_outer_rect.y = y_pos

    @staticmethod
    def rotate_ccw(surface, angle):
        rotated_surface_ccw = pygame.transform.rotozoom(surface, angle, 1)
        rotated_rect_ccw = rotated_surface_ccw.get_rect(center=(611, 125))
        return rotated_surface_ccw, rotated_rect_ccw

    @staticmethod
    def rotate_cw(surface, angle):
        rotated_surface_cw = pygame.transform.rotozoom(surface, angle, 1)
        rotated_rect_cw = rotated_surface_cw.get_rect(center=(611, 124))
        return rotated_surface_cw, rotated_rect_cw

    def update(self, screen):
        ScreenAnimation.angle += 1  # Speed of block decorators
        ScreenAnimation.angle1 += .5  # Speed of half circle decorators
        ScreenAnimation.alpha_no += ScreenAnimation.toggle  # positive or negative direction of tick decorator

        screen.blit(self.image, (0, 0))

        circle_rotated, circle_rect = ScreenAnimation.rotate_cw(self.bg_circle_left, -ScreenAnimation.angle1)
        screen.blit(circle_rotated, circle_rect)
        circle_rotated1, circle_rect1 = ScreenAnimation.rotate_ccw(self.bg_circle_right, ScreenAnimation.angle1)
        screen.blit(circle_rotated1, circle_rect1)
        reflection_rotated, reflection_rect = ScreenAnimation.rotate_ccw(self.bg_reflection, ScreenAnimation.angle1)
        screen.blit(reflection_rotated, reflection_rect)
        reflection_outer_rotated, reflection_outer_rect = ScreenAnimation.rotate_ccw(self.bg_reflection_outer,
                                                                                     -ScreenAnimation.angle1)
        screen.blit(reflection_outer_rotated, reflection_outer_rect)

        blocks_rotated, blocks_rect = ScreenAnimation.rotate_ccw(self.bg_blocks, ScreenAnimation.angle)
        blocks_rotated.set_colorkey((201, 218, 230))
        screen.blit(blocks_rotated, blocks_rect)

        screen.blit(self.bg_ticks, (0, 0))
        self.bg_ticks.set_alpha(ScreenAnimation.alpha_no)

        if ScreenAnimation.alpha_no <= 0:
            ScreenAnimation.toggle = 2
        if ScreenAnimation.alpha_no >= 250:
            ScreenAnimation.toggle = -2

        # test
        screen.blit(images10['grass40x40'], (0, 0))

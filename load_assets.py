#!/usr/bin/python3.9
import pygame
import os


def LoadImages():
    path = '/home/jim/PycharmProjects/sprite_menu/imgs'
    filenames = [f for f in os.listdir(path) if f.endswith('.png')]
    images = {}
    for name in filenames:
        imagename = os.path.splitext(name)[0]
        images[imagename] = pygame.image.load(os.path.join(path, name)).convert_alpha()
    return images

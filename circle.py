#!/usr/bin/en python

import pygame
from pygame.locals import *
import random


class Circle(pygame.sprite.Sprite):

    def __init__(self, xpos, ypos, tile_type, screen):
        super().__init__()
        self.image = pygame.Surface((26, 26))
        pygame.draw.circle(self.image, (20, 50, 20), (13, 13), 13)
        self.image.set_colorkey((0, 0, 0))
        self.rect = self.image.get_rect()
        self.rect.x = xpos
        self.rect.y = ypos
        self.walkable = False
        self.tile_type = tile_type
        self.rectangle_h = pygame.Surface((136, 10))
        pygame.draw.rect(self.rectangle_h, (40, 25, 50), pygame.Rect(0, 0, 136, 10))
        self.rectangle_rect = self.rectangle_h.get_rect()

        self.rectangle_h1 = pygame.Surface((136, 10))
        pygame.draw.rect(self.rectangle_h1, (40, 25, 50), pygame.Rect(0, 0, 136, 10))
        self.rectangle_rect = self.rectangle_h1.get_rect()

        self.rectangle_v = pygame.Surface((10, 136))
        pygame.draw.rect(self.rectangle_v, (25, 40, 50), pygame.Rect(0, 0, 10, 136))
        self.rectangle_rect = self.rectangle_v.get_rect()

        self.rectangle_v1 = pygame.Surface((10, 136))
        pygame.draw.rect(self.rectangle_v1, (25, 40, 50), pygame.Rect(0, 0, 10, 136))
        self.rectangle_rect = self.rectangle_v1.get_rect()

        self.rectangle_t = pygame.Surface((3, 55))
        pygame.draw.rect(self.rectangle_t, (100, 100, 100), pygame.Rect(0, 0, 8, 55))
        self.rectangle_rect = self.rectangle_t.get_rect()

        self.rectangle_t1 = pygame.Surface((88, 3))
        pygame.draw.rect(self.rectangle_t1, (100, 100, 100), pygame.Rect(0, 0, 88, 8))
        self.rectangle_rect = self.rectangle_t1.get_rect()

    @staticmethod
    def circle_surf():
        surf = pygame.Surface((26, 26))
        pygame.draw.circle(surf, (20, 50, 20), (13, 13), 13)
        surf.set_colorkey((0, 0, 0))
        return surf

    def update(self, screen):
        v_val = random.randrange(85, 225, 14)
        h_val = random.randrange(165, 305, 14)
        rand = random.randrange(0, 10)
        if rand > 7:
            screen.blit(self.image, (420, 180), special_flags=BLEND_RGB_ADD)
            screen.blit(self.rectangle_h, (165, v_val), special_flags=BLEND_RGB_ADD)
            #screen.blit(self.rectangle_h1, (165, 100), special_flags=BLEND_RGB_SUB)
            screen.blit(self.rectangle_v, (h_val, 85), special_flags=BLEND_RGB_ADD)
            #screen.blit(self.rectangle_v1, (180, 85), special_flags=BLEND_RGB_SUB)
            screen.blit(self.rectangle_t1, (312, v_val+4), special_flags=BLEND_RGB_ADD)
            screen.blit(self.rectangle_t, (h_val+5, 0), special_flags=BLEND_RGB_ADD)


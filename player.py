#!/usr/bin/ev python

import pygame
from tiles import Tile


class Player(pygame.sprite.Sprite):
    width, height = (40, 40)
    right = False
    left = False
    up = False
    down = False
    collision = False
    h, v = 0, 0
    p_pos = 0
    h_pos = 0
    h_pos_left = 0
    h_pos_right = 0
    v_pos = 0
    v_pos_up = 0
    v_pos_down = 0

    def __init__(self, xpos, ypos):
        super().__init__()
        self.image = pygame.Surface((self.width, self.height))
        self.image.fill((0, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.x = xpos
        self.rect.y = ypos

    def update(self, solid_group):

        self.player_pos()
        if Player.right:
            self.h = 1
        if Player.left:
            self.h = -1
        if Player.up:
            self.v = -20
        if Player.down:
            self.v = 20

    def player_pos(self):
        Player.p_pos = (int(self.rect.x / 40) + (int(self.rect.y / 40) * 20))          # the block the player is on
        Player.h_pos = (int(self.rect.x / 40) + self.h) + (int(self.rect.y / 40) * 20) # the block before or after the player (horizontally)
        Player.h_pos_right = (int(self.rect.x / 40) + 1) + (int(self.rect.y / 40) * 20)
        Player.h_pos_left = (int(self.rect.x / 40) - 1) + (int(self.rect.y / 40) * 20)
        Player.v_pos = (int(self.rect.x / 40) + (int(self.rect.y / 40) * 20) + self.v) # the block before or after the player (vertically)
        Player.v_pos_up = (int(self.rect.x / 40) + (int(self.rect.y / 40) * 20) - 20)
        Player.v_pos_down = (int(self.rect.x / 40) + (int(self.rect.y / 40) * 20) + 20)

    def collide(self, group1):
        if pygame.sprite.spritecollide(self, group1, False):
            print("Hooray---------------------------------------------------------------------------------------------")
            Player.collision = True
